# README #

JMX monitor is jboss mnitoring tool which will fetch Memory Usage ,Connection Pool Statics and Session Count through Web requests to jmx-console


### What is this repository for? ###

### Quick summary ###

JMX Monitor currently have following features

* Fetches Memory .Connection Pool and Session count details
* Supports Authentication
* Supports ssl

### Version###
* Version 1.1 beta

### License###
* MIT


### Dependencies ###
* Python 3+
* Python Libraries : requests , bs4


### Configuration and Usage ###

* Clone the repository
* Create a python virtual environment and activate it.http://python-guide-pt-br.readthedocs.io/en/latest/dev/virtualenvs/
* Go to jmxmonitor directory and install dependent packages by running pip install -r requirement.txt
* Configure Jboss server details on conf/config.py
* run the script using python monitor.py


### Who do I talk to? ###

Repo owner : Jishnu B
Email : jishnub.at@gmail.com