"""Main module which performs the  data fetch"""


'''
    File name: configpy
    Author: Jishnu B
    Date created: 18/07/2017
    Python Version: 3.5
    Licence : MIT, refer LICENCE FILE '''


import requests
from requests.auth import HTTPBasicAuth
from bs4 import BeautifulSoup
from datetime import datetime


class JmxAuth:
	"""Class for fetching data with authentication"""

	def __init__(self,user,passwd,url):
		
		self.user=user
		self.passwd=passwd
		self.url=url

	def check(self):
		"""Method to check for reachability"""

		r=requests.get('%s/jmx-console' %(self.url), auth=HTTPBasicAuth(self.user, self.passwd)) 
		if r.status_code == requests.codes.ok:
			return True
		else:
			return False

	def mem_status(self):
		"""Method to fetch Memory Status"""
		#Fetching Server Info JSP
		r=requests.get('%s/web-console/ServerInfo.jsp' %(self.url), auth=HTTPBasicAuth(self.user, self.passwd))
		mem={}
		#Intializing BS4
		soup = BeautifulSoup(r.text,"html.parser")
		
		#Finding all P Tags in the html and saving needed info to a dict and returning it
		for p in soup.find_all('p'):
			if "Free Memory" in p.text:
				mem['freemem']=p.text
			elif "Max Memory" in p.text:
				mem['maxmem']=p.text
			elif "Total Memory" in p.text:
				mem['totalmem']=p.text
			elif "#Threads" in p.text:
				mem['threads']=p.text

		return mem

	def session_count(self,path):
		"""Method to fetch session_count"""
		#Fetching Session Count JSP
		r=requests.get(self.url+"/jmx-console/HtmlAdaptor?action=inspectMBean&name=jboss.web%3Atype%3DManager%2Cpath%3D%2F"+path+"%2Chost%3Dlocalhost", auth=HTTPBasicAuth(self.user, self.passwd))
		soup = BeautifulSoup(r.text,"html.parser")

		#Finding all td with class sep
		for td in soup.find_all('td',{"class":"sep"}):
			#Finding span tag inside td
			for span in td.find_all('span'):

				'''Finding Span for active Sessions and its next sibling td element 
					and pre tag from the next sibling td.
					pre tag will contain data for seesion count '''

				if span.text=="activeSessions":
					nxtd=td.find_next_sibling('td')
					for pre in nxtd.find_all('pre'):
						return pre.text

	def connection_pool(self,ds):
		"""Method to fetch Connection Pool Data"""

		pool={}
		#Fetching connection pool DATA
		r=requests.get(self.url+'/jmx-console/HtmlAdaptor?action=inspectMBean&name=jboss.jca%3Aservice%3DManagedConnectionPool%2Cname%3D'+ds, auth=HTTPBasicAuth(self.user, self.passwd))
		soup = BeautifulSoup(r.text,"html.parser")

		#Finding value of MaxSize input element
		pool['maxvalue'] = soup.find('input', {'name': 'MaxSize'}).get('value')
		soup = BeautifulSoup(r.text,"html.parser")
		#Finding all td with class sep
		for td in soup.find_all('td',{"class":"sep"}):

			#Finding all td with class sep

			for span in td.find_all('span'):

				'''Finding Span for active In use Connection and its next sibling td element 
					and pre tag from the next sibling td.
					pre tag will contain data for seesion count '''

				if span.text=="InUseConnectionCount":
					nxtd=td.find_next_sibling('td')
					for pre in nxtd.find_all('pre'):
						pool['inused']=pre.text

		return pool
			






class JmxNonAuth:
	"""Class for fetching data without authentication"""

	def __init__(self,url):
		
		self.url=url

	def check(self):
		"""Method to check for reachability"""

		r=requests.get('%s/jmx-console' %(self.url)) 
		if r.status_code == requests.codes.ok:
			return True
		else:
			return False

	def mem_status(self):
		"""Method to fetch Memory Status"""
		#Fetching Server Info JSP
		r=requests.get('%s/web-console/ServerInfo.jsp' %(self.url))
		mem={}
		#Intializing BS4
		soup = BeautifulSoup(r.text,"html.parser")
		
		#Finding all P Tags in the html and saving needed info to a dict and returning it
		for p in soup.find_all('p'):
			if "Free Memory" in p.text:
				mem['freemem']=p.text
			elif "Max Memory" in p.text:
				mem['maxmem']=p.text
			elif "Total Memory" in p.text:
				mem['totalmem']=p.text
			elif "#Threads" in p.text:
				mem['threads']=p.text

		return mem

	def session_count(self,path):
		"""Method to fetch session_count"""
		#Fetching Session Count JSP
		r=requests.get(self.url+"/jmx-console/HtmlAdaptor?action=inspectMBean&name=jboss.web%3Atype%3DManager%2Cpath%3D%2F"+path+"%2Chost%3Dlocalhost")
		soup = BeautifulSoup(r.text,"html.parser")

		#Finding all td with class sep
		for td in soup.find_all('td',{"class":"sep"}):
			#Finding span tag inside td
			for span in td.find_all('span'):

				'''Finding Span for active Sessions and its next sibling td element 
					and pre tag from the next sibling td.
					pre tag will contain data for seesion count '''

				if span.text=="activeSessions":
					nxtd=td.find_next_sibling('td')
					for pre in nxtd.find_all('pre'):
						return pre.text

	def connection_pool(self,ds):
		"""Method to fetch Connection Pool Data"""

		pool={}
		#Fetching connection pool DATA
		r=requests.get(self.url+'/jmx-console/HtmlAdaptor?action=inspectMBean&name=jboss.jca%3Aservice%3DManagedConnectionPool%2Cname%3D'+ds)
		soup = BeautifulSoup(r.text,"html.parser")

		#Finding value of MaxSize input element
		pool['maxvalue'] = soup.find('input', {'name': 'MaxSize'}).get('value')
		soup = BeautifulSoup(r.text,"html.parser")
		#Finding all td with class sep
		for td in soup.find_all('td',{"class":"sep"}):

			#Finding all td with class sep

			for span in td.find_all('span'):

				'''Finding Span for active In use Connection and its next sibling td element 
					and pre tag from the next sibling td.
					pre tag will contain data for seesion count '''

				if span.text=="InUseConnectionCount":
					nxtd=td.find_next_sibling('td')
					for pre in nxtd.find_all('pre'):
						pool['inused']=pre.text

		return pool