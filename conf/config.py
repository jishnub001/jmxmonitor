"""JMX Monitor configuration file"""


'''
    File name: configpy
    Author: Jishnu B
    Date created: 18/07/2017
    Python Version: 3.5
    Licence : MIT, refer LICENCE FILE '''



#IP Address of the Jboss Instance 
IP="52.66.114.42"

#Jboss Listening Port
PORT ="8080"

#Whether SSL is enabled or not , Values = True or False
SSL = False

#Whether JMX-Console requires Authentication , Values=True or False
AUTH= True

#Username of JMX console ..Required only if Auth =True
USER="admin"

#Password for JMX Console ...Required only if Auth =True
PASSWORD="hisLite@Secure"

#List of DataSource for connection pool analysis.Example ['MySqlDS','ClinicalDocDS']
DS_LISTS=['MySqlDS','ClinicalDocDS']

#List of Path for session count analysis.Example ['HIS','HR']
SESSION_PATH=['his']
