#!/usr/bin/env python

"""JmxMoni Monitoring script"""

'''
    File name: monitor.py
    Author: Jishnu B
    Date created: 18/07/2017
    Python Version: 3.5
    Licence : MIT, refer LICENCE FILE '''


#Import Config parameters
from conf.config import *
#Import JmxAuth to fetech data
from conf.jmxauth import JmxAuth,JmxNonAuth

from datetime import datetime

"""Starting Jmx Fetching"""


if SSL:
	url="https://%s:%s" %(IP,PORT)

else:
	url="http://%s:%s" %(IP,PORT)

print ("[+]Starting JMX Data Fetch for %s" %(url))
print("Execution Time : %s" %(datetime.now()))

#If authentication is needed
if AUTH:
	#Creating Object of Jmx AUth class
	fetch=JmxAuth(USER,PASSWORD,url)
	status=fetch.check()

	if status:
		print("%s is reachable" %(url))
	else:
		print("%s is not reachable or authentication failed.Quiting Now" %(url))
		quit()
	#Fetching Memory and thread Details
	memory=fetch.mem_status()

	
	print(memory['freemem'])
	print(memory['maxmem'])
	print(memory['totalmem'])
	print(memory['threads'])

	#Starting Session count fetch
	for path in SESSION_PATH:

		sess=fetch.session_count(path)
		print("Session count for %s  : %s" %(path,sess))

	#Staring Connection pool fetch
	for ds in DS_LISTS:
		con_pool=fetch.connection_pool(ds)
		print("Connection pool  %s Max Size  : %s" %(ds,con_pool['maxvalue']))
		print("Connection pool  %s In Used : %s" %(ds,con_pool['inused']))

	print("[+]Finished \n")

else:

	#Creating Object of Jmx Non AUth class
	fetch=JmxNonAuth(url)
	status=fetch.check()

	if status:
		print("%s is reachable" %(url))
	else:
		print("%s is not reachable .Quiting Now" %(url))
		quit()
	#Fetching Memory and thread Details
	memory=fetch.mem_status()

	
	print(memory['freemem'])
	print(memory['maxmem'])
	print(memory['totalmem'])
	print(memory['threads'])

	#Starting Session count fetch
	for path in SESSION_PATH:

		sess=fetch.session_count(path)
		print("Session count for %s  : %s" %(path,sess))
	print("")

	#Staring Connection pool fetch
	for ds in DS_LISTS:
		con_pool=fetch.connection_pool(ds)
		print(" Connection pool  %s Max Size  : %s" %(ds,con_pool['maxvalue']))
		print("Connection pool  %s In Used : %s" %(ds,con_pool['inused']))

	print("[+]Finished \n")